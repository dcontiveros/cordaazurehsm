## <u>Integrating Azure key vault functionality with Corda - Part 2</u>
Welcome back! This part of the tutorial will focus on setting up the Corda node to interact with the Azure policies you set in the previous installment of this tutorial. Please make sure you have completed that piece first. Let's begin!

## Step 1 - Bootstrap a network (Optional)
Corda is a networked application that requires a participant network to connect to. Most node operators have this info in their node.conf file. However, because this is an end to end tutorial, we will bootstrap a simple network of two nodes. If you already connect to a business network, feel free to grab a cup of ☕ and move onto the next section.

1. Grab the bootstapper jar here:

```
# Grab bootstrapper
wget "http://ci-artifactory.corda.r3cev.com/artifactory/corda-releases/net/corda/corda-tools-network-bootstrapper/4.1/corda-tools-network-bootstrapper-4.1.jar"
```

2. Make a directory called network to house the configs:

```
mkdir network
```

3. Save the following as `NodeA_node.conf`:

```
# Node A - Save as NodeA_node.conf
devMode=true
h2Settings {
    address="localhost:10017"
}
myLegalName="O=NodeA,L=London,C=GB"
p2pAddress="localhost:10005"
rpcSettings {
    address="localhost:10006"
    adminAddress="localhost:10015"
}
security {
    authService {
        dataSource {
            type=INMEMORY
            users=[
                {
                    password=test
                    permissions=[
                        ALL
                    ]
                    user=bankUser
                }
            ]
        }
    }
}
```

4. Save the following as `NodeB_node.conf`:
```
# Node B - Save as NodeB_node.conf
devMode=true
h2Settings {
    address="localhost:11017"
}
myLegalName="O=NodeB,L=London,C=GB"
p2pAddress="localhost:11005"
rpcSettings {
    address="localhost:11006"
    adminAddress="localhost:11015"
}
security {
    authService {
        dataSource {
            type=INMEMORY
            users=[
                {
                    password=test
                    permissions=[
                        ALL
                    ]
                    user=bankUser
                }
            ]
        }
    }
}
```

5. Check that the directory structure looks as follows:

```
network/
├── NodeA_node.conf
└── NodeB_node.conf
```

4. Bootstrap the network with the following command:

```
java -jar corda-tools-network-bootstrapper-4.1.jar --dir network
```

5. Bootstrapping should now be complete. Replace the corda.jar that is populated in Node[AB] directory with the Corda Enterprise jar.
6. Start one node as a test to ensure it works. If you get to the crash shell, the node came up.

Now that we have bootstrapped the network, let's modify the node.conf on NodeA to connect it to Azure.

## Step 2 - Modify az_.conf
We now need to ensure that our Corda node can interface with Azure. The azure options are located here in the Corda docs:

https://docs.corda.r3.com/cryptoservice-configuration.html

1. We need to append the following to node.conf to start with:
```
cryptoServiceName: "AZURE_KEY_VAULT"
cryptoServiceConf: "az_keyvault.conf"
```

2. We now need to populate az_keyvault.conf with the following 5 paramters:

```
path: "/PATH/TO/PKCS12-converted-certificate"
alias: "alias of certificate"
password: "password specified when converting cert to PKCS12 format"
keyVaultURL: "url of keyvault - can be retrieved in azure portal"
clientId: "clientID of Service Principal"
protection: "HARDWARE/SOFTWARE"
```
The options are explained in detail as follows:

#### Path
This is the filepath of the output file of the converted pkcs12 certificate. The easiest way to get this going is to move the pkcs12 cert into the certificates directory of the NodeA or NodeB parent folder. The path is relative to node.conf, but a hard path can be used.

#### Alias
A pkcs12 certificate is an archive file format for storing cryptography objects as a single file. Our pkcs12 file only contains our _ONE_ private/public key pair but we still need to grab the alias name for the cert. We can do this with the following command:

```
keytool -v -list -keystore converted.p12 -storetype pkcs12
```

Example output:
```
Keystore type: PKCS12
Keystore provider: SunJSSE

Your keystore contains 1 entry

Alias name: 1 <---- this goes in your node.conf
```
You can rename this if you have multiple entries in your pkcs12 file, but in this case, 1 is ok.

#### Password
This is the password you specified when you converted the private/public keypair to pkcs12. 

#### keyVaultURL
This is the URL of your keyvault that can be located in the Azure portal. A quick way to get this URL is to run the following command in Powershell:

```
# Generic command
Get-AzKeyVault -Name "VAULT_NAME" | select -ExpandProperty  VaultUri
```

#### ClientId
The new name for this in the Azure portal is `Application ID`. You can access this in the Azure portal by going to the following:

`Azure portal -> Azure Active Directory -> App Registrations`

Or you can run the following command in Powershell:

```
Get-AzADServicePrincipal -DisplayName 'CordaKVTest' | select -ExpandProperty ApplicationId
```

#### Protection
There are two options here: `HARDWARE|SOFTWARE`. These reference soft vs. hard keys. More information is located at the following links:

https://docs.microsoft.com/en-gb/azure/key-vault/
https://github.com/MicrosoftDocs/azure-docs/blob/master/articles/key-vault/about-keys-secrets-and-certificates.md#key-vault-keys

#### Example az_keyvault.conf
Here is how a config should look:

```
# All of these resources have been decommissioned at the time of publication :)
path: "certificates/converted.p12"
alias: "1"
password: "test123"
keyVaultURL: "https://cordakvtest-vault.vault.azure.net/"
clientId: "0b09e2b2-5147-4a17-906d-7fc5ce64adab"
protection: "HARDWARE"
```

Once you've made the appropriate changes, let's start the node.

## Step 3 - Confirming Corda/Azure integration

1. The first step is to delete all the .jks files in your certificates directory. This will ensure that your identity is regenerated.

2. Start Corda as follows:

```
# Let's kick this off in debug mode in order to get relevant errors
java -jar corda.jar --logging-level DEBUG
```
3. Ensure then node gets to the crash shell:

```
[dontiveros@testTLS NodeA]$ java -jar corda.jar --logging-level DEBUG

*************************************************************************************************************************************
*  All rights reserved.                                                                                                             *
*  This software is proprietary to and embodies the confidential technology of R3 LLC ("R3").                                       *
*  Possession, use, duplication or dissemination of the software is authorized only pursuant to a valid written license from R3.    *
*  IF YOU DO NOT HAVE A VALID WRITTEN LICENSE WITH R3, DO NOT USE THIS SOFTWARE.                                                    *
*************************************************************************************************************************************

   ______               __         _____ _   _ _____ _____ ____  ____  ____  ___ ____  _____
  / ____/     _________/ /___ _   | ____| \ | |_   _| ____|  _ \|  _ \|  _ \|_ _/ ___|| ____|
 / /     __  / ___/ __  / __ `/   |  _| |  \| | | | |  _| | |_) | |_) | |_) || |\___ \|  _|
/ /___  /_/ / /  / /_/ / /_/ /    | |___| |\  | | | | |___|  _ <|  __/|  _ < | | ___) | |___
\____/     /_/   \__,_/\__,_/     |_____|_| \_| |_| |_____|_| \_\_|   |_| \_\___|____/|_____|

--- Corda Enterprise Edition 4.0 (8dcaeef) --------------------------------------------------

Tip: If you don't wish to use the shell it can be disabled with the --no-local-shell flag

Logs can be found in                    : /home/username/network/NodeA/logs
! ATTENTION: This node is running in development mode!  This is not safe for production deployment.
Database connection url is              : jdbc:h2:tcp://localhost:10017/node
Advertised P2P messaging addresses      : localhost:10005
RPC connection address                  : localhost:10006
RPC admin connection address            : localhost:10015
Loaded 0 CorDapp(s)                     : 
Node for "NodeA" started up and registered in 32.01 sec


Welcome to the Corda interactive shell.
Useful commands include 'help' to see what is available, and 'bye' to shut down the node.

Fri Jun 28 15:02:11 UTC 2019>>> 
```

4. Check the keyvault in your Azure portal and ensure that you see both keys generated:

![Key-Screenshot](./Images/PortalList.png)

You can also use the following Powershell command:

```
# Generic Command
Get-AzKeyVaultKey -VaultName 'VAULT_NAME'

# Example output
> Get-AzKeyVaultKey -VaultName 'CordaKVTest-vault'

Vault Name     : cordakvtest-vault
Name           : cordaclientca
Version        : 
Id             : https://cordakvtest-vault.vault.azure.net:443/keys/cordaclientca
Enabled        : True
Expires        : 
Not Before     : 
Created        : 6/28/19 3:01:49 PM
Updated        : 6/28/19 3:01:49 PM
Recovery Level : Purgeable
Tags           : 

Vault Name     : cordakvtest-vault
Name           : identity-private-key
Version        : 
Id             : https://cordakvtest-vault.vault.azure.net:443/keys/identity-private-key
Enabled        : True
Expires        : 
Not Before     : 
Created        : 6/28/19 3:01:58 PM
Updated        : 6/28/19 3:01:58 PM
Recovery Level : Purgeable
Tags           : 
```

5. You can also see the type of key being used by going to the key details page:

![KeyInfo](./Images/KeyInfo.png)



You should now be setup with HSM integration. Please feel free to reach to me or our team via the following channels:

Email: opsrel@r3.com
Pubslack: cordaledger.slack.com
Channel: #sysadmin



Thanks! 👋