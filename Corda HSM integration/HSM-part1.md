## <u>Integrating Azure key vault functionality with Corda - Part 1</u>
Corda Enterprise 4.0 brought along support for Azure Key Vault functionality. This is a significant step forward in key management for Corda developers and node operators. This tutorial will walk through setting up a key vault, as well as generating/storing/retriving keys.  This will be a two part series tutorial. The first piece will focus on the Azure setup, while the second will setup Corda properly to store in the HSM.

## Prerequisites for Azure
* Write access to An Azure subscription (To be assigned to Key Vault)
* Write access to an AD Tenant
* Access to a TLS implementation Suite, such as OpenSSL,BoringSSL,LibreSSL (For generating the authentication cert)

## Step 1 - Create a service principal for the entry point
Corda needs to use a service principal as an entry point into Azure. You can create one using the portal as follows:

#### Portal

1. Log into the Azure portal
2. Navigate to Azure Active Directory > App Registration > Click New Registration
3. Fill in the information required (The service principal name we are using is: `CordaKVTest`)


#### Command line
1. Open up Powershell
2. Run the following command:
```
# Ensure you are in the correct Az Context
Get-AzContext

# Create the Service Principal
New-AzADServicePrincipal  -DisplayName "CordaKVTest"
```

This will create the required Service Principal. 


## Step 2 - Assign a role to the new Service Principal
To allow the Service Principal access to our future key vault, we need to assign it a role. A nice blanket role for this use case is PENDING.

To assign this role do the following:

1. Log into the Azure portal
2. Navigate to Subscriptions > (Your Azure subscription in use for this exercise) > Access Control (IAM) > Add
3. Select PENDING role > Type your Service principal name in the Select box (CordaKVTest)


We have now allocated a role assignment to your Service principal. We now need to generate a certificate for authentication to lock down access.

## Step 3 - Generate a certificate that will be used for authentication
You will need access to your TLS implementation suite for this step. To enable cert based authentication in Azure you need to generate the following:

1. A private key based on RSA (Rivest–Shamir–Adleman), with a recommended key size of at least 2048
2. An X.509 public key/certificate based off the previous public key
3. The ability for your TLS suite to convert the above two results into pkcs12 format.

The instuctions differ for each TLS suite, but as long as you can meet the above requirements, the certificate should be valid. The following instructions are for OpenSSL:

```
# Generate a private key of 4096-bit size
openssl genrsa -out private.key 4096

# Generate X.509 public key that expires in 2 years
# This will prompt you fill in information.
openssl req -new -x509 -key private.key -out publickey.cer -days 730

# Convert into pkcs12 (for Corda to read)
# Enter a password. You will need this pass for Corda configuration
openssl pkcs12 -export -out converted.p12 -inkey private.key -in publickey.cer
```
You will need to upload `publickey.cer` into Azure so be sure to transfer it to a location you have access to. We will proceed with this next.

## Step 4 - Associate the certificate to the Service Principal
The certificate we created in step 3 must now be uploaded so the Service Principal can use it for auth:
1. Navigate to Azure Active Directory > App Registrations > Application Name (CordaKVTest)
2. Click certificates and secrets
3. Under the Certificates section, click "Upload Certificate"
4. Select publickey.cer, and click Add.
5. Validate the thumbprint. If you used OpenSSL, the following command will output the thumbprint so you can validate easily:
```
# The azure portal uses a sha-1 thumbprint.
# The awk/tr commands format the thumbprint for copy/paste validation
openssl x509 -noout -fingerprint -sha1 -inform pem -in publickey.cer  | awk -F '=' '{print $2}' | tr -d ':'
```

## Step 5 - Create the key vault on Azure portal

This is a fairly easy task. You can do the following:

1. Log into the Azure portal
2. Click All services
3. Search for key vaults and select.
4. Once the key vault blade is shown, Click add
5. Fill out the information, but ensure that the Premium option is selected. This ensures we have an HSM backed key. The name we will use for this exercise is CordaKVTest-vault.